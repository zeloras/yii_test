<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Category;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $model = new Category();
        return $this->render('index', ['model' => $model]);
    }

    /**
     * Displays Add Menuitem Page.
     *
     * @return string
     */

    public function actionAdd_category() {
        $model = new Category();
        if ($model->load(Yii::$app->request->post()) && $model->createCategory()) {
            return $this->redirect('/');
        }

        return $this->render('category_action', ['model' => $model]);
    }
    
    /**
     * Displays Edit Menuitem Page.
     *
     * @return string
     */

    public function actionEdit_category($item = 0) {
        $model = new Category();
        $getData = $model->find()->where(['=', 'id', $item])->one();

        if ($model->load(Yii::$app->request->post()) && $getData->updateCategory()) {
            return $this->refresh();
        }

        return $this->render('category_action', ['model' => $getData]);
    }
    
    
    public function actionDelete_category($item = 0) {
        $model = new Category();
        $getData = $model->find()->where(['=', 'id', $item])->one();

        if (sizeof(Yii::$app->request->post()) > 0 && $model->deleteCategory($item)) {
            return $this->redirect('/');
        }

        return $this->render('category_delete', ['model' => $getData]);
    }
}
