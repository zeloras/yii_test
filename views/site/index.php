<?php

/* @var $this yii\web\View */

$this->title = 'Yii Test task';
$menu = $model->getCategory();

function buildHtmlMenu($menu) {
    $data = '<ul class="test-menu">';

    foreach ($menu as $item) {
        $has_open_link = (sizeof($item['tree']) > 0) ? '<a href="javascript:;" class="test-menu__toggle"></a>' : '';
        $data .= '<li class="test-menu__item">' . $has_open_link . '<a class="test-menu__link" href="' . $item['link'] . '">' . $item['name'] . '</a> <a href="/edit/' . $item['id'] . '">(edit)</a> <a href="/delete/' . $item['id'] . '">(delete)</a>';
        if (sizeof($item['tree']) > 0) {
            $data .= buildHtmlMenu($item['tree']);
        }

        $data .= '</li>';
    }
    return $data . '</ul>';
}

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <?= buildHtmlMenu($menu)?>
            </div>
        </div>
    </div>
</div>
