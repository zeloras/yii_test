<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$form = ActiveForm::begin([
    'id' => 'add-menu',
    'options' => ['class' => 'form-horizontal'],
]);
?>
<h2>Delete category: <?=$model->name?></h2>
<h2>Are you sure?</h2>
<div class="form-group">
    <div class="col-6">
        <?= Html::button('Cancel', ['class' => 'btn', 'onclick' => 'document.location.href="/"']) ?>
        <?= Html::submitButton('Delete', ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>