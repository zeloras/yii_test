<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$form = ActiveForm::begin([
    'id' => 'add-category',
    'options' => ['class' => 'form-horizontal'],
]);

print $form->field($model, 'id')->hiddenInput()->label(false);
print $form->field($model, 'name');
print $form->field($model, 'link');
print $form->field($model, 'parent')->dropDownList($model->getCategorySelect());
?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>