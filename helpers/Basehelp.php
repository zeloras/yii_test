<?php
namespace app\helpers;

class Basehelp {

    /**
     * Format tree elements
     * @param array $items, integer $parent unformat list
     * @return array formated list
     */
    public static function formatTree(array &$items, $parent = 0) {
        $list = array();

        foreach ($items as $key => $item) {
            if ((int)$item['parent'] == (int)$parent) {
                unset($items[$key]);
                $item['tree'] = self::formatTree($items, (int)$item['id']);
                $list[] = $item;
            }
        }
        return $list;
    }
}

