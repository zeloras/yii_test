<?php

namespace app\models\db;

use yii\db\ActiveRecord;

/**
 * UpdateMenu create and edit menu items
 */
class Category extends ActiveRecord
{

    /**
     * Set current tablename
     * @return string
     */
    public static function tableName() {
        return 'category';
    }
    
    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['name', 'parent', 'link'], 'required'],
            [['name', 'parent', 'link', 'id'], 'safe'],
            ['parent', 'integer'],
        ];
    }
    
    /**
     * @return array attrs.
     */
    public function attributes() {
        return ['name', 'parent', 'link', 'id'];
    }

    /**
     * @return array labels
     */
    public function attributeLabels() {
        return [
            'parent' => 'Parent category',
            'name' => 'Category name',
            'link' => 'Link'
        ];
    }
}
