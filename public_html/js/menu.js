var menu_list = new Object({
    init: function () {
        var $self = this;
        var $menu_list = document.querySelectorAll('.test-menu__toggle');
        if ($menu_list.length > 0) {
            for (var $menu of $menu_list) {
                $menu.addEventListener('click', function (e) {
                    $self.changeState(this);
                }, false);
            }
        }
    },
    
    changeState: function (item) {
        var $currentItem = new Mime(item.closest('li'));

        if ($currentItem.hasclass('test-menu__state_open')) {
            $currentItem.removeclass('test-menu__state_open');
        } else {
            $currentItem.addclass('test-menu__state_open');
        }
    }
});

menu_list.init();