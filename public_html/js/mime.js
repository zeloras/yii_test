function removeclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        obj.setAttribute("class", $attrib);
    }
}

function hasclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        var reg = new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i");
        $attrib = reg.test($attrib);
        return $attrib;
    } else {
        return false;
    }
}

function addclass(elem, obj) {
    var $attrib = obj.getAttribute("class");
    if ($attrib !== null) {
        $attrib = $attrib.replace(new RegExp("^" + elem + "$|\\s" + elem + "$|^" + elem + "\\s|\\s" + elem + "\\s", "i"), "")
        $attrib = $attrib + " " + elem;
        obj.setAttribute("class", $attrib);
    }
}

var Mime = function(item) {
    this.hasclass = function(elem){return hasclass(elem, item);};
    this.addclass = function(elem){return addclass(elem, item);};
    this.removeclass = function(elem){return removeclass(elem, item);};
};